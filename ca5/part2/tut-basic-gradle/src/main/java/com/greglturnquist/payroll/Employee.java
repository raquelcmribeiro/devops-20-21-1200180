/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.util.Objects;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

    private @Id
    @GeneratedValue
    Long id; // <2>
    private String firstName;
    private String lastName;
    private String description;
    private String jobTitle;
    private String emailAddress;

    public Employee() {
    }

    /**
     * Constructor method for Employee class.
     * @param firstName first name of the employee
     * @param lastName last name of the employee
     * @param description description of the employee
     * @param jobTitle job title of the employee
     * @param email e-mail of the employee
     */
    public Employee(String firstName, String lastName, String description, String jobTitle, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.description = description;
        this.jobTitle = jobTitle;
        this.emailAddress = email;
        validate();
    }

    /**
     * Method to validate the email format.
     */
    private void validate(){
        validateContent();
        validateEmailFormat();
    }

    /**
     * Method to validate e-mail content.
     */
    private void validateContent() {
        if (this.firstName == null || this.firstName.isEmpty() || this.firstName.trim().length() == 0) {
            throw new IllegalArgumentException("First name is not valid.");
        }
        if (this.lastName == null || this.lastName.isEmpty() || this.lastName.trim().length() == 0) {
            throw new IllegalArgumentException("Last name is not valid.");
        }
        if (this.description == null || this.description.isEmpty() || this.description.trim().length() == 0) {
            throw new IllegalArgumentException("Description is not valid.");
        }
        if (this.jobTitle == null || this.jobTitle.isEmpty() || this.jobTitle.trim().length() == 0) {
            throw new IllegalArgumentException("Job title is not valid.");
        }
        if (this.emailAddress == null || this.emailAddress.isEmpty() || this.emailAddress.trim().length() == 0) {
            throw new IllegalArgumentException("E-mail Address is not valid.");
        }
    }

    /**
     * Method to validate e-mail format.
     */
    private void validateEmailFormat() {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";
        Pattern pattern = Pattern.compile(emailRegex);
        if (!pattern.matcher(this.emailAddress).matches()) {
            throw new IllegalArgumentException("E-mail Address doesn't have a valid format.");
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(description, employee.description) &&
                Objects.equals(jobTitle, employee.jobTitle) &&
                Objects.equals(emailAddress, employee.emailAddress);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName, description, jobTitle, emailAddress);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getEmail() {
        return emailAddress;
    }

    public void setEmail(String email) {
        this.emailAddress = email;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", description='" + description + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", email='" + emailAddress + '\'' +
                '}';
    }
}
// end::code[]
