import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void createAnEmployeeSuccessfullyWithAllValidData() {
        String firstName = "Sam";
        String lastName = "Gamgee";
        String description = "Gardener and protector";
        String jobTitle = "Gardener";
        String emailAddress = "sam@lordrings.com";

        Employee employee = new Employee(firstName, lastName, description, jobTitle, emailAddress);

        assertNotNull(employee);
    }

    @Test
    void failToCreateAValidEmployee_NullFirstName() {
        String firstName = null;
        String lastName = "Gamgee";
        String description = "Gardener and protector";
        String jobTitle = "Gardener";
        String emailAddress = "sam@lordrings.com";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailAddress));
    }

    @Test
    void failToCreateAValidEmployee_EmptyLastName() {
        String firstName = "Sam";
        String lastName = "";
        String description = "Gardener and protector";
        String jobTitle = "Gardener";
        String emailAddress = "sam@lordrings.com";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailAddress));
    }

    @Test
    void failToCreateAValidEmployee_EmptyDescription() {
        String firstName = "Sam";
        String lastName = "Gamgee";
        String description = "    ";
        String jobTitle = "Gardener";
        String emailAddress = "sam@lordrings.com";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailAddress));
    }

    @Test
    void failToCreateAValidEmployee_NullJobTitle() {
        String firstName = "Sam";
        String lastName = "Gamgee";
        String description = "Gardener and protector";
        String jobTitle = null;
        String emailAddress = "sam@lordrings.com";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailAddress));
    }

    @Test
    void failToCreateAValidEmployee_EmptyEmail() {
        String firstName = "Sam";
        String lastName = "Gamgee";
        String description = "Gardener and protector";
        String jobTitle = "Gardener";
        String emailAddress = "";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailAddress));
    }

    @Test
    void failToCreateAValidEmployee_InvalidEmailFormat() {
        String firstName = "Sam";
        String lastName = "Gamgee";
        String description = "Gardener and protector";
        String jobTitle = "Gardener";
        String emailAddress = "wrongEmailFormat.com";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, emailAddress));
    }
}