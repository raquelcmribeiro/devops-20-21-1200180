# Class Assignment 5 Report - Part 2

## CI/CD Pipelines with Jenkins

This is a technical report that presents the implementation of the requirements
for the second part of this assignment using [Jenkins](https://www.jenkins.io/).

Jenkins was originally called Hudson and it is an open source Continuous Integration 
tool written in Java. It provides an easy-to-use continuous integration system, 
easier for developers to integrate project changes, and easier to obtain a fresh build. 
The automated, continuous build increases the productivity.

### Features:

- Automated testing (Scheduling)
- Notifications (Email notification for a build project)
- Reporting plugins (such as Junit)
- Code Analysis plugins
- Automated Deployment: Plugins available to transfer the build files after a 
  successful build to the respective application/web server
- Metrics & Trends plugins
- Server Maintenance (Shutdown, Restart, Reload the configuration, Backup)
- Security: Ability to setup users and their relevant permissions on the Jenkins instance
- Distributed Builds/Testing

###
# 1. Analysis, Design and Implementation - Jenkins

The goal of the Part 2 of this assignment is to create a pipeline in Jenkins to 
build the tutorial spring boot application, gradle "basic" version (developed in 
CA2, Part2) of this bitbucket [Repository](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180).


## Create Jenkinsfile

You should define the following stages in your pipeline:

- *Checkout*. To checkout the code from the selected repository.
  

- *Assemble*. Compiles and produces the archive files with the application. 
  
> Do not use the build task of gradle (because it also executes the tests).
  

- *Test*. Executes the Unit Tests and publish in Jenkins the Test results. 
  
> See the junit step for further information on how to archive/publish test results.
> Also, add the **junit plugin** to Jenkins.


- *Javadoc*. Generates the javadoc of the project and publish it in Jenkins. 
  
> See the publishHTML step for further information on how to archive/publish html 
> reports.


- *Archive*. Archives in Jenkins the war files generated during Assemble. 
  


- *Publish Image*. Generate a docker image with Tomcat and the war file and publish it
  in the Docker Hub. 

> See **https://jenkins.io/doc/book/pipeline/docker/**, in section Building Containers.
> In the example, docker.build will build an image from a Dockerfile in the same 
> folder as the Jenkinsfile. The tag for the image will be the job build number of Jenkins.

###
Now, the Jenkinsfile should look like this:

```groovy
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Check out step.'
                git credentialsId: 'credentials', url: 'https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assemble step.'
                dir('ca5/part2/tut-basic-gradle') {
                    sh './gradlew assemble'
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Test step'
                dir('ca5/part2/tut-basic-gradle') {
                    sh './gradlew test'
                }
            }
            post {
                always{
                    junit '**/build/test-results/test/*.xml'
                }
            }
        }
        stage('Javadoc') {
            steps {
                echo 'Javadoc step'
                dir('ca5/part2/tut-basic-gradle') {
                    sh './gradlew javadoc'
                    publishHTML (target: [
                            reportName: "Javadoc Report",
                            reportDir: 'build/docs/javadoc',
                            reportFiles: 'index.html',
                            keepAll: true,
                            alwaysLinkToLastBuild: true,
                            allowMissing: true
                    ])
                }
            }
        }
        stage('Archive') {
            steps {
                echo 'Archive step.'
                archiveArtifacts '**/build/libs/*'
            }
        }
        stage('Publish Image'){
            steps{
                echo 'Publish Docker image step.'
                dir('ca5/part2/tut-basic-gradle') {
                    script{
                        docker.withRegistry('https://registry.hub.docker.com', 'docker-hub-credentials'){
                            def customImage = docker.build("raquelcmribeiro/devops-images:${env.BUILD_ID}")
                            customImage.push()
                        }
                    }
                }
            }
        }
    }
}
```
##
## Configure Dockerfile

In the project's folder, configure the following Dockerfile, that will add the generated 
war file inside the container:

```dockerfile
FROM tomcat

RUN apt-get update -y
RUN apt-get install -f
RUN apt-get install git -y
RUN apt-get install nodejs -y
RUN apt-get install npm -y
RUN mkdir -p /tmp/build

ADD /build/libs/tut-basic-gradle-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080
```

###
### Set up credentials for DockerHub

Just like it was described in the Part 1 of CA5, store the credentials for the Docker 
Hub log in, with the ID used in Jenkinsfile (**docker-hub-credentials**):

![image](ca5-part2-images/credentials.PNG)

###
## Create a new Pipeline Job

As previously explained, create a new Pipeline give it a name and the repository 
path and then define the new **Script Path** for the Jenkinsfile:

![image](ca5-part2-images/scriptPath.PNG)

###
## Build

If everything went well the build will succeed in all its steps :

![image](ca5-part2-images/jenkinsBuild.PNG)

You can consult the details of the steps in the pipeline jobs through the logs:

- **Checkout** step, cloning the repository and using the given credentials:

![image](ca5-part2-images/stepCheckout.PNG)

###
- **Assemble** step, where the jar file was generated:

![image](ca5-part2-images/stepAssemble1.PNG)

![image](ca5-part2-images/stepAssemble2.PNG)

###
- **Test** step, where the tests were run, and the test report was produced:

![image](ca5-part2-images/stepTest.PNG)

We can check the report in Jenkins:

![image](ca5-part2-images/testsPassed.PNG)

Or check the html version:

![image](ca5-part2-images/testReport.PNG)

###
- The generating of **Javadoc** step:

![image](ca5-part2-images/stepJavaDoc.PNG)

The html version:

![image](ca5-part2-images/javadocReport.PNG)

![image](ca5-part2-images/javadocReportEmployee.PNG)

###
- The **Archive** of the artifacts step:

![image](ca5-part2-images/stepArchive.PNG)

![image](ca5-part2-images/artifacts.PNG)

###
- And finally, the step for **Publishing** the Docker image:

![image](ca5-part2-images/stepPublish1.PNG)

![image](ca5-part2-images/stepPublish2.PNG)

![image](ca5-part2-images/stepPublish3.PNG)

###
Check in your repository if the image has been pushed with the tag corresponding 
to the *Build Id*:

![image](ca5-part2-images/publishedImage.PNG)

###
Additionally, you can see if the image runs successfully in Docker Desktop:

![image](ca5-part2-images/dockerRunImage.png)

##

# 2. Analysis of the alternative - Buddy

[Buddy](https://app.buddy.works/) is a web-based and self-hosted continuous integration 
and delivery (CI/CD) software for Git developers used to build, test and deploy websites 
and applications. It is a very practical and easy-to-understand tool that 
can handle all configurations of the integration process through an appealing and 
intuitive panel interface, avoiding some more complex concepts.

Main advantages:

- Very quick configuration of the pipelines: 15-minute setup

- Wide range of predefined ready-to-use actions that can be arranged 
  in any way
  
- Simple installation

- Easy to learn with a telling GUI
  
- Easily integrates with AWS, Google, Docker, and Kubernetes accounts


### Comparison with Jenkins

Buddy has the same concepts as Jenkins. It has the workspace, pipelines and Buddy's 
actions are the same as Jenkins' build steps.

Regarding setup and configuration of the project builds,  Buddy has a 15-minute 
configuration via GUI with instant export to YAML files while Jenkins setup can 
take 6-10 hours as every action requires a dedicated plugin that needs to be 
troubleshot for functionality.

In terms of performance on Buddy the dependencies and Docker layers are cached in 
isolated containers, so it is better optimized. To improve performance in Jenkins,
the plugins have to be manually adjusted, or the machine upgraded.

And finnaly, comparing the maintenance criteria, Buddy has an intuitive UI/UX that 
allows non-tech users to create and manage pipelines. Jenkins requires a full-time 
DevOps engineer to configure, monitor and manage pipelines.

##

# 3. Implementation of the Alternative - Buddy

So now, the steps for the same implementation of the CA5 - Part 2 tasks in **Buddy**.
The used files are located in the 
[alternative](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180/src/master/ca5/part2/alternative/) 
folder in this repository.


### Create account and configure Bitbucket repository

- First, the simplest way is to go to [Buddy web app](https://buddy.works/sign-up) 
and sign up an account.
  

- Once registered, Buddy automatically detects all the Git accounts for the 
associated e-mail and asks to choose a repository to sync:

![image](ca5-part2-images/syncRepository.PNG)

###
### Add a new Pipeline

- Now, for your projects, you can simply add a Pipeline:

![image](ca5-part2-images/addNewPipeline.PNG)

###
- Set some simple configurations for your pipeline:

![image](ca5-part2-images/newPipeline.PNG)

###
Now, in your pipeline, you can choose two options to configure the actions flow:

- Through previously defined templates with actions:

![image](ca5-part2-images/templates.PNG)

###
- Or choosing each action available:

![image](ca5-part2-images/actions.PNG)

##
### Define the actions for the Pipeline

Now you can mount your Pipeline steps (actions) to achieve the tasks for this assignment.

- First, the **Checkout step** is already done, because Buddy already has the 
  repository synched, and you don't need a specific action to clone it.
  
  
- For the **Assemble step**, just choose a *Gradle* action, and type the commands 
to accomplish the task. In this case, the instructions to enter the project folder, 
to change gradlew permissions, and the gradle command to assemble:

![image](ca5-part2-images/stepAssemble.PNG)

###
- In the **Test step**, choose a *Gradle* action, and type the corresponding 
Gradle commands to run tests in the project:
  
![image](ca5-part2-images/alt-stepTest.PNG)

###
- Similarly, for the **Javadoc step**, again add a *Gradle* action and type the 
  corresponding commands to generate the Javadoc:

![image](ca5-part2-images/alt-stepJavadoc.PNG)

###
- In the **Archive step**, choose the *Archive* action and provide the path to the 
  files you want to archive, output target path and target file name:

![image](ca5-part2-images/alt-stepArchive.PNG)

###
Finally, for building and publishing a **Docker Image**, Buddy has two separate actions 
available:

- To build the Docker image, add the *Build Docker Image* action and provide the  
path to the dockerfile in your project.

![image](ca5-part2-images/stepBuildImage1.PNG)

> In this case, we used the Dockerfile that contains the Build of the project inside 
> it because it had no problems finding gradlew file with this alternative tool to 
> achieve all steps.
> 

- Also, in Options menu, define the Docker Hub account credentials:

![image](ca5-part2-images/stepBuildImage2.PNG)

###
- Then, to push the Docker Image to the DockerHub choose the *Push Docker to a Registry* 
action and configure it with the registry saved credentials, repository name and 
tag name for the generated image:
  
![image](ca5-part2-images/stepPushImage.PNG)

##
### Run the Pipeline

Now, will all the actions configured, just run the Pipeline by just clicking a 
button:

![image](ca5-part2-images/runPipeline.png)

##
### Build Results

Once completed the pipeline, we can see in a graphic and intuitive way the summary 
of all the steps, status and logs of each action performed.

![image](ca5-part2-images/BuildComplete.PNG)

###
So we can see in more detail each one:

- For the **Assemble**, we can check in the *Filesystem*, under build/libs folder 
the generated .war file:

![image](ca5-part2-images/assembleWarFile.PNG)

###
- For the **Tests**, also in the *Filesystem* we see the generated reports files:
  
![image](ca5-part2-images/filesystemTests.PNG)

###
- And the generated XML files under build/test-results/test folder:

![image](ca5-part2-images/testXML.PNG)

###
- In the browser, we can check the Test Report for our project:

![image](ca5-part2-images/alt-testReport.PNG)

###
- Regarding the **Javadoc**, we can consult the logs information about the action:

![image](ca5-part2-images/logsJavadoc.PNG)

###
- And also check the correspondent html files in Filesystem:

![image](ca5-part2-images/javadocFiles.PNG)

###
- And see the correspondent Javadoc report in the browser:

![image](ca5-part2-images/alt-javadocReport.PNG)

###
- For the **Archive** action we can also check the logs information:

![image](ca5-part2-images/logsArchive.PNG)

###
- In the last part, regarding the **Docker Image** it is possible to see all the 
steps and actions inside the Dockerfile through the command line logs:

![image](ca5-part2-images/logsBuildImage.PNG)

###
- Finally, we can see that the last action was completed successfully, and we can 
check the repository in DockerHub to see the pushed image through this Pipeline action:

![image](ca5-part2-images/dockerHubPushed.PNG)

##
### Optional: YAML configuration

All this steps above showed that using this tool through its *Magic UI* is really 
quick, easy and effective, but **Buddy** has another configuration mode for the 
Pipelines: yaml configuration files.

![image](ca5-part2-images/yamlConfiguration.PNG)

###
So you can add your customized configuration for your jobs through a yaml file, 
called by convention **buddy.yml** in the root of your project.

Buddy also allows you to convert an already configured pipeline (in UI mode) to a 
yaml file, which in this case (for the CA5 tasks) would be: 

```yaml
- pipeline: "CA5-PART2-ALTERNATIVE"
  on: "CLICK"
  refs:
    - "refs/heads/master"
  priority: "NORMAL"
  always_from_scratch: true
  fail_on_prepare_env_warning: true
  actions:
    - action: "Assemble Step"
      type: "BUILD"
      working_directory: "/buddy/devops-20-21-1200182"
      docker_image_name: "library/gradle"
      docker_image_tag: "5.4-jdk8"
      execute_commands:
        - "cd ca5/part2/tut-basic-gradle/"
        - "chmod +x gradlew"
        - "./gradlew assemble"
      volume_mappings:
        - "/:/buddy/devops-20-21-1200182"
      shell: "SH"
    - action: "Test step"
      type: "BUILD"
      working_directory: "/buddy/devops-20-21-1200182"
      docker_image_name: "library/gradle"
      docker_image_tag: "5.4-jdk8"
      execute_commands:
        - "cd ca5/part2/tut-basic-gradle"
        - "./gradlew test"
      volume_mappings:
        - "/:/buddy/devops-20-21-1200182"
      shell: "SH"
    - action: "Javadoc Step"
      type: "BUILD"
      working_directory: "/buddy/devops-20-21-1200182"
      docker_image_name: "library/gradle"
      docker_image_tag: "5.4-jdk8"
      execute_commands:
        - "cd ca5/part2/tut-basic-gradle/"
        - "./gradlew javadoc"
      volume_mappings:
        - "/:/buddy/devops-20-21-1200182"
      shell: "SH"
    - action: "Archive step"
      type: "ZIP"
      local_path: "ca5/part2/tut-basic-gradle/build/libs/tut-basic-gradle-0.0.1-SNAPSHOT.war"
      destination: "ca5/part2/tut-basic-gradle/build/libs/tut-basic-gradle-0.0.1-SNAPSHOT.war.zip"
    - action: "Build Docker image"
      type: "DOCKERFILE"
      dockerfile_path: "ca5/part2/alternative/Dockerfile"
      integration_hash: "ANR8M7Vl4zbq6ZvRdprOZqEgL3"
    - action: "Push Docker Image Step"
      type: "DOCKER_PUSH"
      docker_image_tag: "builtInBuddy"
      repository: "raquelcmribeiro/devops-images"
      integration_hash: "ANR8M7Vl4zbq6ZvRdprOZqEgL3"
```

##

============================================

*This work was developed by **Raquel Ribeiro [1200180]**.*