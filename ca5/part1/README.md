# Class Assignment 5 Report - Part 1

## CI/CD Pipelines with Jenkins

This is a technical report that presents the implementation of the requirements
for the first part of this assignment using [Jenkins](https://www.jenkins.io/), 
an open source automation server that provides hundreds of plugins to support 
building, deploying and automating any project.

## CA5-1 Tasks:

The goal of the Part 1 of this assignment is to practice with Jenkins using the *gradle
basic demo* project made in ca2-part1 of this bitbucket 
[Repository](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180).

## 1. Install Jenkins

- See instructions [here](https://jenkins.io/doc/book/installing/) and download 
  the WAR file.
- Execute the following command in a terminal:

```bash
java -jar jenkins.war
```

- While running open **http://localhost:8080** to access Jenkins and execute any 
  necessary additional setup step. Configure it with the default plugins.

## 2. Create Jenkinsfile

You should define the following stages in your pipeline:

- *Checkout*. To checkout the code from the selected repository.
  

- *Assemble*. Compiles and produces the archive files with the application. 
  
> Do not use the build task of gradle (because it also executes the tests).
  

- *Test*. Executes the Unit Tests and publish in Jenkins the Test results. 
  
> See the junit step for further information on how to archive/publish test results.
> Also, add the **junit plugin** to Jenkins.
  

- *Archive*. Archives in Jenkins the files generated during Assemble.

The Jenkinsfile should look like this:

```groovy
pipeline {
   agent any

   stages {
      stage('Checkout') {
         steps {
            echo 'Check out step.'
            git credentialsId: 'credentials', url: 'https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180'
         }
      }
      stage('Assemble') {
         steps {
            echo 'Assemble step.'
            dir('ca5/part1/gradle_basic_demo') {
               sh './gradlew assemble'
            }
         }
      }
      stage('Test') {
         steps {
            echo 'Test step'
            dir('ca5/part1/gradle_basic_demo') {
               sh './gradlew test'
            }
         }
         post {
            always{
               junit '**/build/test-results/test/*.xml'
            }
         }
      }
      stage('Archive') {
         steps {
            echo 'Archive step.'
            archiveArtifacts '**/build/libs/*'
         }
      }
   }
}
```
## 3. Create a new Job in Jenkins

### Set up credentials

In order to access and checkout a private repository, the correspondent credentials 
should be provided. For that Jenkins allows you to configure that information:

- Follow **Manage Jenkins** menu.
- Then **Manage Credentials** menu.
- Choose **Global Credentials**.
- **Add Credentials** with your repository username and password.
- Also, set your ID which is identified from jobs and other configurations where 
those credentials will be used - in this case it is called *credentials*.

### Create a new Pipeline

- Create a new job of type **Pipeline** a give it a name.


- Select the definition: Pipeline script from SCM (*Source Code Manager*), put the 
repository URL and define the credentials (previously set):

![image](ca5-part1-images/pipeline.PNG)

- Define the **Script Path** for the Jenkinsfile located in the target project:

![image](ca5-part1-images/scriptPath.PNG)


## 4. Build

Once everything is configured, you can Build the pipeline job:

![image](ca5-part1-images/buildNow.PNG)

### Results

If everything went well, and the build was successful, you can see the results and 
Jenkins shows a nice weather. You can consult the details of the steps in the 
pipeline jobs through the logs:

- Checkout step, cloning the repository and using the given credentials:

![image](ca5-part1-images/stepCheckout.PNG)

- Assemble step, where the jar file was generated:

![image](ca5-part1-images/stepAssemble.PNG)

- Test step, where the tests were run, and the test report was produced:

![image](ca5-part1-images/stepTest.PNG)

We can check the report in Jenkins:

![image](ca5-part1-images/passedTest.PNG)

Or check the html version:

![image](ca5-part1-images/reportTest.PNG)

- And finally, the archive step:

![image](ca5-part1-images/stepArchive.PNG)


![image](ca5-part1-images/buildArtifactsJAR.PNG)


##

============================================

*This work was developed by **Raquel Ribeiro [1200180]**.*