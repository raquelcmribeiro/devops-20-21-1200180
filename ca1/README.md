# Class Assignment 1 Report

## Version Control With Git

This is a technical report that presents the analysis and description of the 
process behind the implementation of a scenario illustrating a Git workflow and 
also using an alternative tool.

## A. Analysis, Design and Implementation - *Git*

**Git** is a free and open source distributed version control system that was created 
in 2005, and it is designed to handle from small to very large projects with speed 
and efficiency.

### Bitbucket repository

The source code for this assignment is located in the folder ca1/tut-basic in the
[Bitbucket repository](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180/src/master/).

## CA1 Assignment Tasks - Git:

### 1. Have a master branch to publish stable versions

Type **git status** command to ensure that you are on the master branch.

```bash
> git status
On branch master
Your branch is up to date with 'origin/master'.
```
Also, with this command you can list the files you've changed, and those you 
still need to add or commit.

  

### 2. Tag the initial version

Use **git tag (tagname)** command to create tags for the last commit.

```bash
> git tag -a v1.2.0 -m "initial version of project for ca1"
```

>**git tag**: lists all the tags.

> **git tag (tagname) (commitID)**: creates the tag for the specific commit.

>flag **-a**: creates an annotated tag.

>flag **-m**: adds a message to the tag.

Then, push the tag to remote.

```bash
> git push origin v1.2.0
```
  

### 3.  Develop new features in branches

### 3.1  Create the branch *email-field*

Use **git branch (branchname)** command to create a new branch.

```bash
> git branch email-field
```
Then checkout to that branch.

```bash
> git checkout email-field
Switched to branch 'email-field'
```
>**git branch**: lists all the branches.

>flag **-b**: creates the branch and checkout at the same time.

>flag **-d**: deletes the branch.

Then push and set the branch remote as upstream:

```bash
> git push --set-upstream origin email-field
To https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180.git
* [new branch]      email-field -> email-field
  Branch 'email-field' set up to track remote branch 'email-field' from 'origin'.
```
### 3.2 Add support for email field

Now you can edit your project locally, adding support for the new email feature 
and testing for validation of the Employees attributes. Debug the server and 
client parts of the solution.

### 3.3 Merge new feature with master

- First, run **git status** to verify which files have changed and which you still 
need to add or commit.
  
- Then use **git add .** to begin tacking untracked files or to stage files 
  (previously tracked but modified) so they all become ready for the next commit.

- Then use **git commit** to commit changes; it records the snapshot you set up 
  in your staging area.
  
```bash
> git commit -m "Add testing for validation of Employee atributtes"
```

>flag **-m**: adds a message to the commit.

>flag **-a**: automatically stages every changed file (skipping the git add)

- And then type **git push** to send the changes to your remote branch.

```bash
> git push origin email-field
```
- Now, for merging this changes into master branch, first you have to move to 
  master:

```bash
> git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.
```
- Finally, bring the changes from your branch using **git merge**:

```bash
> git merge email-field
Updating 895d97e..975b59d
Fast-
(...)
30 files changed, 237 insertions(+), 120 deletions(-)
```
- And send it to remote with **push** command.
```bash
> git push origin master
```

### 3.4 Tag new version

Tag this new project version, following the steps previously described.

```bash
> git tag -a v1.3.0 -m "Employees with validations"
```
```bash
> git push origin v1.3.0
To https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180.git
 * [new tag]         v1.3.0 -> v1.3.0
```
  

### 4.  Fix bugs in branches

Follow the steps described in Section 3.

- Create and checkout new branch *fix-invalid-email* and send to remote:

```bash
> git branch -b fix-invalid-email
```
```bash
> git push --set-upstream origin fix-invalid-email

```
- Edit your project and fix the bugs - allowing only valid email addresses.

- Commit changes locally, and push them to remote branch:

```bash
> git commit -m "Fix email format validation"
```

```bash
> git push origin fix-invalid-email
```
- Merge changes with local master and send to remote:

```bash
> git checkout master
```
```bash
> git merge fix-invalid-email
```
```bash
> git push origin master
```
- Tag the new fixed version:

```bash
> git tag -a v1.3.1 -m "Fixed email validation"
```
```bash
> git push origin v1.3.1
```
  

### 5.  Final tag for the assignment
At the end of the assignment, mark your repository with the tag ca1 and send it to remote:
```bash
> git tag -a ca1 -m "ca1 final tag"
```
```bash
> git push origin ca1
```
  

### 6.  Other useful Git commands

>**git init**: creates a subdirectory .git that contains all repository files 
> (nothing is tracked yet).

> **git clone (link)**: clones a remote repository.

> **git diff**: to see what's staged or unstaged.

> **git log**: lists commits history.

> **git rm**: removes files from being tracked (staging area).

> **git fetch**: downloads the data (remote) to local repository.

> **git pull**: fetches and merges new updates from remote into current work 
> (git fetch + git merge). Very important command to use when you're working 
> in a team in the same repository.

  

## B. Analysis of the alternative - *Subversion*

**Subversion (SVN)** is an open source version control system created in 2000 as 
a project of the Apache Software Foundation.

Subversion repositories are similar to Git repositories, but there are some 
differences regarding the architecture of the projects.

The commits in a SVN project are organized into specific subdirectories:


- trunk
  

- branches
  

- tags

The trunk directory represents the most recent stable version of a project which 
is equivalent to master in Git. The active development work is developed with 
subdirectories in branches. When a resource is completed, its directory is merged 
into trunk and then removed. 
Git projects are stored in a single directory and the complete history of all 
branches and tags are stored within the .git directory. The latest stable version 
is contained in the standard branch, and the active work is developed in separate 
branches. When a resource is completed, the resource branch is merged into the 
standard branch and deleted.

The Git working directory structure (where edit the files are edited, locally) 
remains the same, but the contents of the files are changed according to the current 
branch - there is only one branch in the working directory at a time.
Subversion's checkouts are different: they combine the repository data in working 
directories making a copy for each one of them, so there is a working directory 
for each branch and tag. In repositories with many branches and tags, checking out 
everything can overload the bandwidth.

Another important difference is that Git allows a developer to work on the project 
offline while in SVN offline features are very limited.

Also in Subversion, commits have simple incremental ID numbers while in Git the 
commit ID is a set of forty hexadecimal characters that specify a 160-bit SHA-1 hash.

  

## C. Implementation - *Subversion*

### Remote repository

The source code for this assignment is located in the folder trunk/tut-basic in the
[Helix Team Hub repository](https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/devops-20-21-1200180/tree/HEAD).

### SVN commands to set repository

To send changes from the working copy to remote repository:

- Use the **svn checkout (link)** command to check out a working copy from the 
  repository. This command can be shortened to **svn co**.

```bash
> svn checkout https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/trunk
```
*(this sets the local repository in your computer, creating a .svn subdirectory)*

> Don't forget to create the SVN project structure, using the command **mkdir** to 
> create the *trunk*, *branches* and *tags* folders.

- Type **svn add** command to add the new files.
```bash
> svn add *
```

- Finally, make **svn commit** to commit and send changes (new files) back to the server.

```bash
> svn commit -m "Initial commit"
```

*Comparing with Git, we have similar steps, like adding files to the staging area 
with the add command. But the major difference here, is that the commit command in
SVN performs a commit and also sends the changes to remote (without a push command).*

  

## CA1 Assignment Tasks - SVN:

### 1. Have a master branch to publish stable versions

The **svn status** command prints the status of working copy files and directories.

```bash
> svn status
```
The **svn update** / **svn up** command updates your working copy with changes from 
the repository:

```bash
> svn update trunk
```
  

### 2. Tag the initial version
In Subversion creating a tag is made the same way as creating a new branch - it 
creates a new copy of the project, but they are used for different reasons: tags 
are typically used to create a static snapshot of the project at a particular 
stage. The branches are actually used for development and for editing that copy.

Use **svn copy** command to create a new tag (or branch):

```bash
> svn copy https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/trunk
https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/tags/v1.2.0
      -m "add tag v1.2.0"
```
*(this creates a copy of the trunk folder into a new folder - v1.2.0 - under tags directory)*

  

### 3.  Develop new features in branches

### 3.1  Create the branch *email-field*

Use **svn copy** command to create a new branch:

```bash
> svn copy https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/trunk
https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/branches/email-field
      -m "create new branch email-field"
```
*(this creates a copy of the trunk folder into a new folder, email-field, under branches directory)*


Then checkout and update the branch content to start working:

```bash
> svn checkout https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/branches/email-field
```
```bash
> svn up email-field
```

### 3.2 Add support for email field

Now you can edit your project locally, adding support for the new email feature
and testing for validation of the Employees attributes. Debug the server and
client parts of the solution.

### 3.3 Merge new feature with master

- First, run **svn status** to verify which files are new to be added.

- Then use **svn add** to stage files new files.

- Then use **svn commit** to commit changes and send them to remote branch folder.

```bash
> svn commit -m "added email field and tests for validations"
```
*(branch folder email-field is now updated with new content in remote repository)*

- Now, for merging this changes branch, first you have to checkout to trunk:
```bash
> svn checkout https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/trunk
```

- The **svn merge** command combines two different versions into your working copy:

```bash
> svn merge https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/branches/email-field
```

- At last, you have to send this new changes in trunk that are in your local working 
  copy to the remote trunk with a commit:
  
```bash
> svn commit -m "add email-field branch merge locally and send to remote trunk"
```

### 3.4 Tag new version

Tag this new project version, following the steps previously described.

```bash
> svn copy https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/trunk
https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/tags/v1.3.0
      -m "add tag for version v1.3.0"
```
  

### 4.  Fix bugs in branches
Take the same steps described in Section 3.

- Create and checkout new branch *fix-invalid-email*, update it and checkout:

```bash
> svn copy https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/trunk
https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/branches/fix-invalid-email
      -m "create new branch to fiz invalid email"
```
```bash
> svn checkout https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/branches/fix-invalid-email
```
```bash
> svn up fix-invalid-email
```
- Edit your project and fix the bugs - allowing only valid email addresses.

- Commit changes to store in remote branch:

```bash
> svn commit -m "add extra validation for email format and updated testing"
```

- Merge changes with local trunk and send to remote:
```bash
> svn checkout https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/trunk
```
```bash
> svn merge https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/branches/fix-invalid-email
```
```bash
> svn commit -m "merged fix-invalid-email branch to trunk and send it to remote"
```

- Tag the new fixed version:
```bash
> svn copy https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/trunk
https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/tags/v1.3.1
      -m "add tag for version v1.3.1"
```
  

### 5.  Final tag for the assignment
At the end of the assignment, mark your repository with the tag ca1:
```bash
> svn copy https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/trunk
https://helixteamhub.cloud/raquelribeiro/projects/devops-20-21-1200180/repositories/subversion/devops-20-21-1200180/tags/ca1
      -m "add tag ca1 - end of the assignment"
```
  

### 6.  Other useful SVN commands

>**svn delete**: deletes a file from your working copy of the repository.

> **svn diff**: reveals the differences between your working copy and the trunk 
> in SVN repository.

> **svn log**: shows log messages from the repository.

> **svn revert**: reverts changes in your working copy.

> **svn shelve**: stores your changes without submitting them.

  
  
  

============================================

*This work was developed by **Raquel Ribeiro [1200180]**.*