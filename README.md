# Individual Repository for DevOps

### [1200180]  Raquel Ribeiro

This repository contains the files for all the class assignments of DevOps:

* [Class Assignment 1](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180/src/master/ca1/)
  

* [Class Assignment 2 - Part 1](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180/src/master/ca2/part1/)


* [Class Assignment 2 - Part 2](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180/src/master/ca2/part2/)


* [Class Assignment 3 - Part 1](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180/src/master/ca3/part1/)


* [Class Assignment 3 - Part 2](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180/src/master/ca3/part2/)


* [Class Assignment 4](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180/src/master/ca4/)


* [Class Assignment 5 - Part 1](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180/src/master/ca5/part1)


* [Class Assignment 5 - Part 2](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180/src/master/ca5/part2)
