# Class Assignment 3 Report - Part 2

## Virtualization with Vagrant

This is a technical report that presents the implementation of the requirements
for the second part of this assignment using Vagrant.

The source code for this assignment is located in the folder ca3/part2 in this
[Bitbucket repository](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180/src/master/).

###

**Vagrant** is an open source software for creating and maintaining portable 
virtual development environments. It tries to simplify software configuration 
management for virtualizations in order to increase development productivity.


# 1. Analysis, Design and Implementation - Vagrant + VirtualBox

The goal of Part 2 of this assignment is to use Vagrant to setup a virtual environment
to execute the tutorial spring boot application, gradle "basic" version developed in
[CA2-Part2](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180/src/master/ca2/part2/).



### Install and initialize Vagrant

Download and install the latest stable version of Vagrant [here](https://www.vagrantup.com/downloads).

To ensure everything went well, type the following command to check the installed version :

```bash
vagrant -v
```
![image](ca3images2/vagrantVersion.PNG)

Create a new folder to initialize a vagrant project and move to that folder:

```bash
mkdir vagrant-project-assignment
```
```bash
cd vagrant-project-assignment
```

### Vagrantfile

Use the Vagrantfile in the following repository as the initial solution: https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/

Two Virtual Machines are going to be created and provisioned:

- **web**: this VM is used to run Tomcat and the Spring boot basic application.

- **db**: this VM is used to execute the H2 server database.


Copy the Vagrantfile into your repository under ca3/part2/vagrant-project-assignment.

## Update the Vagrantfile configuration

### 

- ### React and Spring Data REST application

The copied Vagrantfile is previously configured to use another application.  Some 
changes have to be made, so it can use your own gradle version of the spring project.

To do that, in your project, in the *build.gradle* file add the plugin for support 
of building the war file:

```groovy
id 'war'
```

In the dependencies section, add:

```groovy
providedRuntime 'org.springframework.boot:spring-boot-startet-tomcat'
```

In your project, add a new java class ServletInitializer:

```java
package com.greglturnquist.payroll;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }
}
```

Go to *application.properties* under resources folder and add the following:

```properties
server.servlet.context-path=/tut-basic-gradle-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
#spring.datasource.url=jdbc:h2:mem:jpadb
#In the following settings the h2 file is created in /home/vagrant folder
spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
#So that spring will not drop the database on every execution
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```

In the *app.js* file, make sure you add the correct application context path:

```js
componentDidMount() { // <2>
		client({method: 'GET', path: '/tut-basic-gradle-0.0.1-SNAPSHOT/api/employees'}).done(response => {
			this.setState({employees: response.entity._embedded.employees});
		});
	}
```

In the *index.html* file remove an additional backslash before main.css:

```html
<link rel="stylesheet" href="main.css" />
```

Finally, **build** the project (as seen in previous tutorials) with the new changes 
and send it to remote repository.
## 

- ### Vagrantfile adaptation

The box used to initialize VMs configured in this file is **envimation/ubuntu-xenial**.
It supports VirtualBox and VMWare.

Change the Vagrantfile and adapt it to your project:

```shell
# Change the following command to clone your own repository!
git clone https://raquelcmribeiro@bitbucket.org/raquelcmribeiro/devops-20-21-1200180.git
cd devops-20-21-1200180/ca2/part2/tut-basic-gradle
chmod u+x gradlew
./gradlew clean build
# To deploy the war file to tomcat8 do the following command:
sudo cp ./build/libs/tut-basic-gradle-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
SHELL
```

Make sure Vagrant clones your repository where your version of the Spring project is 
and make it enter the right folder with the application. Also don't forget to change 
the name of the build war file in every place that it is being used.
You may need to set you repository to **public**.

## Start Vagrant

Now, it's all set to work with Vagrant in your project. To deploy the two VMs 
(**db** and **web**) in the vagrant-project-assignment folder, in the terminal:

```bash
vagrant up
```

This will look for the Vagrantfile and execute according to the configurations for 
the application's frontend and the H2 database. The first run will take some time 
because both machines will be provisioned with several software packages:

- Downloads the base Vagrant box
- Calls the hypervisor (VirtualBox) to create a VM from that box
- Installs hypervisor tools as needed (VirtualBox tools) and setups the network and
- shared folders
- Runs provision scripts to prepare the VM
- Setups SSH keys to login to the VM

To check the VMs running, just type:

```bash
vagrant status
```

![image](ca3images2/vagrantStatus.PNG)

And you can also see them running in VirtualBox Manager GUI:

![image](ca3images2/virtualboxRunning.PNG)

You can connect to each VM with the command:

```bash
vagrant ssh db
```
or
```bash
vagrant ssh web
```

## Open Application

In the host you can open the spring web application using one of the following options:

- http://localhost:8080/tut-basic-gradle-0.0.1-SNAPSHOT/
- http://192.168.33.10:8080/tut-basic-gradle-0.0.1-SNAPSHOT/

You can also open the H2 console using one of the following urls:

- http://localhost:8080/tut-basic-gradle-0.0.1-SNAPSHOT/h2-console
- http://192.168.33.10:8080/tut-basic-gradle-0.0.1-SNAPSHOT/h2-console
  
> For the connection string use: **jdbc:h2:tcp://192.168.33.11:9092/./jpadb**

### 

You can see something like this:

![image](ca3images2/vagrantFrontend.PNG)

And in the H2 database console:

![image](ca3images2/vagrantDB.PNG)

## Other Vagrant useful commands

### Stop VM

The command to stop the VM is:

```bash
vagrant halt
```

![image](ca3images2/vagrantHalt.PNG)

### Create VM

to initialize Vagrant with a Vagrantfile and ./.vagrant directory:

```bash
vagrant init
```
> Before you can do vagrant up, you'll need to specify a base image in the Vagrantfile.

To initialize Vagrant with a specific box:

```bash
vagrant init <boxpath>
```
> To find a box, go to the public [Vagrant box catalog](https://app.vagrantup.com/boxes/search). 
> Just replace the name with boxpath, for example, **vagrant init envimation/ubuntu-xenial**.


### Cleanup

To stop and delete all traces of the vagrant machine:

```bash
vagrant destroy
```

> with the flag **-f** to force the destruction without confirmation.


### Reload

To restarts vagrant machine and load a new Vagrantfile configuration:

```bash
vagrant reload
```

Restart the virtual machine and force provisioning:

```bash
vagrant reload --provision
```

# 2. Analysis of the alternative - Hyper V

**Microsoft Hyper-V** was released in 2008 and it is formerly known as Windows Server 
Virtualization. The Hyper-V is free a product of Microsoft and can be created by 
systems like Windows and many other of their distributions.

The hypervisor is a type 1 hypervisor (hardware-level virtualization), meaning that
it runs directly on the machine's hardware to create virtual machines.


### Hyper V and VirtualBox comparison

Hyper-V and VirtualBox are both free and attractive solutions for virtualization. 
Hyper-V is a type 1 hypervisor, hosted in a formal operating system: when the host 
computer starts it takes control from BIOS and then it starts managing the operating 
system. Virtual machines can be started manually by user or automatically depending 
on its settings. 


VirtualBox is a type 2 hypervisor (hosted virtualization): it runs on the operating 
system installed on the host. When a physical computer starts, the host OS loads and 
takes control. The user starts the hypervisor application and then starts the needed 
virtual machines.

![image](ca3images2/virtualizationTypes.PNG)

Hyper-V is always on if the host is powered on, while the VirtualBox can be started 
and closed by a user on demand.

Hyper-V can only be installed on Windows-based systems, while VirtualBox is a 
multiplatform product, it supports a high number of guest and host operating systems, 
and hence, is suitable for multiplatform environments.

VirtualBox is used especially to work with sound, USB and a very wide range of supported 
OS. Hyper-V is designed to host servers where a lot of extra desktop hardware isn't needed 
(USB for example). Hyper-V is usually faster than VirtualBox in a lot of scenarios.

###

> For this assignment, both technologies were successfully implemented to full extent. 
> Both were well integrated and managed through **Vagrant**.

# 3. Implementation of the Alternative - Hyper V


### Install Hyper V

To install [Hyper V](https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v) 
you just need to enable it on Windows 10 (only for Enterprise, Pro or 
Education editions) without installing any third-party software.

- Right click on the Windows button and select ‘Apps and Features’
- Select Programs and Features on the right under related settings
- Select Turn Windows Features on or off.
- Select Hyper-V and click OK.

![image](ca3images2/alternativeInstall.PNG)

Once the installation has completed, you are prompted to restart your computer.

### Set new Vagrant project and Vagrantfile

Create a new folder to initialize the alternative vagrant project (this time for Hyper V) 
and move to that folder:

```bash
mkdir vagrant-hyperv-alternative
```
```bash
cd vagrant-hyperv-alternative
```

Use the same Vagrantfile for the initial solution configured to run your project with 
VirtualBox. Copy it to the new alternative project folder.

## Update the configuration

### 

- ### Vagrantfile adaptation

The box that will be used to initialize VMs with Hyper V is **generic/ubuntu1604**.
It supports the providers: Hyper V, VirtualBox, VMWare, Parallels and Libvirt.

The *Vagrantfile* is already suited to your Spring and Data REST project, however now 
you have to add a different box and configure Hyper V:

```shell
Vagrant.configure("2") do |config|
    config.vm.box = "generic/ubuntu1604"
    config.vm.provider "hyperv"
```

The *config.vm.provider* call will make Hyper V the default provider. 

Alternatively, you can run the command in the terminal to force the provider, 
everytime you need to start the virtual machines:

```bash
vagrant --provider=hyperv
```

In the configurations specific to the database VM, change accordingly:

```shell
config.vm.define "db" do |db|
db.vm.box = "generic/ubuntu1604"
db.vm.hostname = "db-alternative"
```

Also in the configurations specific to the webserver VM:
```shell
config.vm.define "web" do |web|
web.vm.box = "generic/ubuntu1604"
web.vm.hostname = "web-alternative"

# We set more ram memmory for this VM
    web.vm.provider "hyperv" do |h|
      h.memory = 1024
    end
```

## Start Vagrant

To intialize the two VMs with the Hyper V, go the vagrant-hyperv-alternative folder and
in the command line type:

```bash
vagrant up
```

This will read the new configured Vagrantfile and execute according to the specifications for
the application's frontend and the H2 database, now starting two VM in Hyper V.

To check the VMs running, just type:

```bash
vagrant status
```

![image](ca3images2/alternativeStatus.PNG)

And you can also see them running in Hyper V Manager GUI:

![image](ca3images2/alternativeRunning.PNG)

### IP configuration

Now, because the machines weren't defined with static IP, before we can access the 
application, we need to check the generated IP for our machines.
First the VM **db**:

![image](ca3images2/alternativeIPdb.PNG)

Then with this address, just update it in the *application.properties* file:

- Connect to VM **web** in the terminal, by running the command:

```bash
vagrant ssh web
```

- Enter the cloned repository in the machine and access *resources* folder in the project:

```bash
cd devops-20-21-1200180/ca2/part2/tut-basic-gradle/src/main/resources
```

- Open and edit the file:

```bash
nano application.properties
```

- And edit the line:

```properties
spring.datasource.url=jdbc:h2:tcp://172.24.108.40:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FAL$
```

### Reload Vagrant

Every time a new virtual machine is created a new random IP is generated. So the next 
step is to reload the machines keeping its IP addresses. For that, we need to force 
the provision in the reload command so that the VM reboots and runs the provision steps:

```bash
vagrant reload --provision
```

## Access Application

To acess the application's frontend, you'll need to check the IP of the **web** VM:

![image](ca3images2/alternativeIPweb.PNG)

In the host you can open the spring web application using one of the following options:

- http://localhost:8080/tut-basic-gradle-0.0.1-SNAPSHOT/
- http://172.24.104.46:8080/tut-basic-gradle-0.0.1-SNAPSHOT/

You can also open the H2 console using one of the following urls:

- http://localhost:8080/tut-basic-gradle-0.0.1-SNAPSHOT/h2-console
- http://172.24.104.46:8080/tut-basic-gradle-0.0.1-SNAPSHOT/h2-console

> For the connection string use: **jdbc:h2:tcp://172.24.108.40:9092/./jpadb**

### 

You can see something like this:

![image](ca3images2/alternativeWEB.PNG)

And in the H2 database console:

![image](ca3images2/alternativeDB.PNG)


##

============================================

*This work was developed by **Raquel Ribeiro [1200180]**.*