# Class Assignment 3 Report - Part 1

## Virtualization with Vagrant

This is a technical report that presents the implementation of the requirements
for the first part of this assignment using VirtualBox.

**Virtualization** is a technique that allows a computer to emulate other 
computers and other hardware devices, that is implemented by a hypervisor.

A **hypervisor** creates and runs virtual machines. The virtual machine is composed 
by the virtual hardware resources, the guest operating system, and the set of applications 
that can run on it. The hypervisor can run directly on the machine's hardware 
(*Hardware-level virtualization*) or within a formal operating system (*Hosted virtualization*).


For the assigment it will be used [VirtualBox VM](https://www.virtualbox.org), a software 
that provides a hosted virtualization and can be installed and run in multiple OS.

## CA3 Tasks:

The goal of the Part 1 of this assignment is to practice with VirtualBox using the same
projects from the previous assignments but now inside a VirtualBox VM with Ubuntu.

## 1. Install VirtualBox and create VM

First step is to install the Virtualbox virtualization software in the computer.
Then start by creating the VM as described in the *Hands-On* in the lecture.

### Create VM and configure settings

1. In VirtualBox, select the menu **Machine** and choose **New...**


2. Name the VM and choose the guest OS: **Linux**


3. Download the image **mini.iso** (Ubuntu 18.04 "Bionic Beaver") from
   https://help.ubuntu.com/community/Installation/MinimalCD.
   

4. Change settings for the created VM:
    - The VM need 2048 MB RAM.
    - In Settings -> Network: set Network Adapter 1 as Nat (already set as default)
    - In Settings -> Network: enable a Network Adapter 2 as Host-only Adapter.
    - In Settings -> storage: insert the mini.iso into the optical drive.
    
> Set **Live CD/DVD** so that when VM is initialized the iso image is read and loaded 
> so Linux can be installed.

5. Create a host-only network:
    - Select menu File -> Host Network Manager.
    - Click the create button (Ctrl +A) and a new Host-only network will be created and 
      added to the list. Select *Enable* in DCHP Server.
    - Check the IP address range of this network. It should be **192.168.56.1/24.**

### Start VM and Install Ubuntu

Start the VM in the **Start** icon and the installation of Ubuntu will run automatically 
from the virtual optical drive. Install and configure preferences as a normal OS 
install process.

### Setup networking

After starting the VM, log on into the VM and continue the setup:

Update the packages repositories:

```bash
sudo apt update
```

Install the network tools:
```bash
sudo apt install net-tools
```

Edit the network configuration file to setup the IP:
```bash
sudo nano /etc/netplan/01-netcfg-yaml
```

And add the **anp0s8 address**:
 ```yaml
 network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s3:
     dhcp4: yes
    enp0s8:
     addresses:
       - 192.168.56.5/24
```

> Respect all the spaces and indentation!

> **Ctrl+O** to save and **Ctrl+X** to exit the file.


Apply the new changes:

```bash
sudo netplan apply
```

Type the command **ifconfig** to verify:

![image](ca3images1/ifconfig.PNG)


Install openssh-server so that we can use ssh to open secure terminal sessions to the
  VM (from other hosts):

```bash
sudo apt install openssh-server
```

To enable password authentication for ssh, open file:

```bash
sudo nano /etc/ssh/sshd_config
```

And uncomment the line _PasswordAuthentication yes_.

Then, restart service ssh:
```bash
sudo service ssh restart
```

Check status with the command **service ssh status**:

![image](ca3images1/sshServiceStatus.PNG)


- Install an ftp server to use the FTP protocol to transfers files to/from
  the VM (from other hosts):

```bash
sudo apt install vsftpd
```

To enable write access for vsftpd, open file:
```bash
sudo nano /etc/vsftpd.config
```

And uncomment the line _write_enable=YES_.
Then, restart service vsftpd:

```bash
sudo service vsftpd restart
```

Check status with the command **service ssh status**:

![image](ca3images1/vsftpdServiceStatus.PNG)


### Connect to the VM

Since the SSH server is enabled in the VM, use ssh to connect the host to the VM.

Open a terminal (host operating system) and type:
```bash
ssh name@192.168.56.5
```
> *name* is the configured username in the VM.
> 192.168.56.5 should be replaced by the IP of the VM (if different).

Now it's connected:

![image](ca3images1/connectVM.PNG)

Since the FTP server is also enabled in the VM use an ftp application, like
[FileZilla](https://filezilla-project.org) to transfer files to/from the VM.


### Install Git and Java

Simply type the commands:

```bash
sudo apt install git
```
```bash
sudo apt install openjdk-8-jdk-headless
```

## 2. Test the Spring Tutorial application

Make a clone of the application **tut-react-and-spring-data-rest** with the 
following command:

```bash
git clone https://github.com/spring-guides/tut-react-and-spring-data-rest.git
```

Change to the directory of basic version:

```bash
cd tut-react-and-spring-data-rest/basic
```

Build and execute the application with maven wrapper command:

```bash
./mvnw spring-boot:run
```

To open the application use the browser in the host computer and the correct to 
**http://192.168.56.5:8080/**, using the IP of the VM.


![image](ca3images1/SpringTutorialRun.PNG)

## 3. Cloning individual repository inside the VM

Change to the root directory, and clone the remote repository into the VM.

```bash
git clone https://raquelcmribeiro@bitbucket.org/raquelcmribeiro/devops-20-21-1200180.git
```
![image](ca3images1/clonerepository.PNG)

Check the cloned repository in FileZilla:

![image](ca3images1/filezillaRepository.PNG)

## 4. Build and execute applications

## tut-basic app

Try to run the tut-basic project, located in CA1 folder inside the VM, entering 
the app folder:

```bash
cd devops-20-21-1200180/ca1/tut-basic
```

Before running, allow executable permissions (x) to *mvnw* file:

![image](ca3images1/mvnwPermissions.PNG)

And then run the application through maven wrapper command:

```bash
./mvnw spring-boot:run
```

In **http://192.168.56.5:8080** the application will run smoothly:

![image](ca3images1/tut-basicRun.PNG)


## gradle_basic_demo app

Now try to run the gradle_basic_demo project, located in CA2/part1 folder inside the 
VM, entering the app folder:

```bash
cd devops-20-21-1200180/ca2/part1/gradle_basic_demo
``` 

Allow executable permissions to *gradlew* file:

![image](ca3images1/gradlewPermissions.PNG)

And then run the application through gradle wrapper command:

```bash
./gradlew build
```

![image](ca3images1/gradleBuild.PNG)

Run the gradle task to run the server on port **59001**:

```bash
./gradlew runServer
```

![image](ca3images1/runServer.PNG)

With the server running in the VM, we can now run the client on the host computer.

> Do not try to run the client in the VM because it doesn't have a Desktop (Ubuntu server).

Open a new command line in the host operating system and go to the project's folder.

Try to run the task runClient. 

It fails the connection because it will try to reach the host address but that does 
not have the server running. We need to configure a new task to make the client reach 
the VM running server through the Host-Only ethernet adapter.

```groovy
task runClientForVM(type:JavaExec, dependsOn: classes){
   group = "DevOps"
   description = "Launches a chat client that connects to a server on VM 192.168.56.5:59001"

   classpath = sourceSets.main.runtimeClasspath

   main = 'basic_demo.ChatClientApp'

   args '192.168.56.5', '59001'
}
```

Now, run the client through the new task and a new chat window is created:

```bash
./gradlew runClientForVM
```

![image](ca3images1/runClientForVM.PNG)


##

============================================

*This work was developed by **Raquel Ribeiro [1200180]**.*