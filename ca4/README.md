# Class Assignment 4 Report

##  Containers with Docker

This is a technical report that presents the implementation of the requirements
for the forth assignment using Docker.

The source code for this assignment is located in the folder **ca4/docker-task** in this
[Bitbucket repository](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180/src/master/ca4/docker-task/).

###

**Containers** allow creating isolated environments for applications while sharing 
a single operating system. Containers are significantly more lightweight than VMs 
because they occupy less space, and they are easier to create and use while still 
offering a good level of isolation and other benefits of VMs.

**Docker** is a tool that allows developers to easily deploy applications in 
containers to run on the host operating system. The main benefit of using Docker 
is that it allows users to package an application with all of its dependencies 
into a standardized unit for software development. Unlike virtual machines, 
containers use much less computer resources because they are fast and lightweight.

The operation mode, main components and architecture of Docker can be seen below:

![image](ca4-images/architecture.png)


# 1. Analysis, Design and Implementation - Docker

The goal of this assignment is to use Docker to setup a containerized environment to
execute your version of the gradle version of spring basic tutorial application developed in
[CA2-Part2](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180/src/master/ca2/part2/).



### Install Docker

Download Docker [here](https://www.docker.com/) and install it.

To check information about Docker and about containers running, type the following 
command:

```bash
docker info
```

![image](ca4-images/dockerInfo.PNG)


## Organize project structure

Similarly to previous assignment with Vagrant, now we need to use Docker to set up 
2 containers/services:

- web: container used to run Tomcat and the Spring application
- db: container used to execute the H2 server database

As an example, use this [repository](https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/) 
to follow the specified structure:

![image](ca4-images/projectStructureCompose.PNG)

For that, create the folder **db** and generate a Dockerfile:

```dockerfile
FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists
```

Then, create the folder **web** and with the Dockerfile:

```dockerfile
FROM tomcat

RUN apt-get update -y
RUN apt-get install -f
RUN apt-get install git -y
RUN apt-get install nodejs -y
RUN apt-get install npm -y
RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://raquelcmribeiro@bitbucket.org/raquelcmribeiro/devops-20-21-1200180.git

WORKDIR /tmp/build/devops-20-21-1200180/ca2/part2/tut-basic-gradle

RUN chmod u+x gradlew

RUN ./gradlew clean build

RUN cp build/libs/tut-basic-gradle-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080
```

To define and run multi-container applications we need to specify images and 
configuration in a simple YAML file. Create it in the root folder and add the 
following configuration for both containers:

```dockerfile
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.33.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data
    networks:
      default:
        ipv4_address: 192.168.33.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.33.0/24
```

## Run Docker

To get the containers up and running combined in our single host machine, just 
use the command (in the folder containing **docker-composer.yml** file):

```bash
docker-compose up
```
This accomplishes the following steps:

- Builds images from Dockerfiles
- Pulls images from registries
- Creates and starts containers
- Configures a bridge network for the containers
- Handles logs

You'll see Spring application running:

![image](ca4-images/dockerComposeUpSpring.PNG)

Also, in Docker Desktop App you'll be able to have a more graphic view of the 
containers running:

![image](ca4-images/dockerComposeUpRunningUI.PNG)

### Useful commands

To build the images:
```bash
docker-compose build
```

To list images:
```bash
docker-compose images
```

As an example, see:

![image](ca4-images/dockerImages.PNG)

To start/Stop the containers - initiating or stopping processes:
```bash
docker-compose start/stop <service>
```

To list the containers:
```bash
docker-compose ps
```

To remove service containers (making impossible to restart):
```bash
docker-compose rm <service>
```

## Open Application

With the Docker and the containers running, in the host you can open the spring 
web application using the following url:

 - http://localhost:8080/tut-basic-gradle-0.0.1-SNAPSHOT/

You can also open the H2 console using the following url:

 - http://localhost:8080/tut-basic-gradle-0.0.1-SNAPSHOT/h2-console
  
> For the connection string use: **jdbc:h2:tcp://192.168.33.11:9092/./jpadb**

### 

You can see something like this:

![image](ca4-images/dockerComposeUpFrontend.PNG)

And in the H2 database console:

![image](ca4-images/dockerComposeUpH2Test.PNG)

![image](ca4-images/dockerComposeUpH2Data.PNG)


## Publish images to Docker Hub

To publish the images to the registry in [DockerHub](https://hub.docker.com/):

- First create an account and sign up.


- Create a new repository.


- In the command line, do the login using the command:

```bash
docker login
```

![image](ca4-images/dockerLogin.PNG)

- Tag the image, using:

```bash
docker tag <image_name> <repository_name>:<tag>
```

- And then push it to Docker Hub:

```bash
docker push <repository_name>:<tag>
```

See the examples for **db** and **web** images:

![image](ca4-images/dockerPushImageWeb.PNG)

![image](ca4-images/dockerPushImageDb.PNG)

The images pushed to the repository:

![image](ca4-images/dockerHubImagesPushed.PNG)


> The created repository in the scope of this assignment is available in this 
> [link](https://hub.docker.com/repository/docker/raquelcmribeiro/devops-images).


## Copy database file to Volume

Now using the Volume in the db container **/usr/src/data** we can get a copy of 
the database file into our host machine.

With the containers running, open a new command line and type the following command
 to enter the container **docker-task_db_1**:

```bash
docker exec -it docker-task_db_1 bash
```

![image](ca4-images/dockerExecDB.PNG)

The current working directory inside the container is **/usr/src/app**. To copy 
the database  to the data directory, use:

```bash
cp jpadb.mv.db /usr/src/data
```

And check if it was successfully copied:

![image](ca4-images/copyDBFile.PNG)

On the host side, you can check that the copied file produced another copy in the 
Volume data folder, in the project's root:

![image](ca4-images/copiedDBFileToVolume.PNG)

You can also check it through the terminal:

![image](ca4-images/copiedDBFileToVolume2.PNG)

##

# 2. Analysis of the alternative - Kubernetes

[Kubernetes](https://kubernetes.io/) is a portable, extensible, open-source platform, 
developed by Google in 2014, for managing containerized workloads and services, 
that facilitates both declarative configuration and automation of the manual processes 
involved in deploying, managing and scaling applications. It has a large, rapidly 
growing ecosystem. 

Containers pack services into one unit which can be deployed anywhere — local machine,
test environment or production systems. In a production environment, piling up 
containers into one machine can be a problem because it’ll run out of capacity. Multiple 
machines, also called nodes, should be running in a cluster (networked and talking 
to each other) and deploy these containers to them.

![image](ca4-images/Docker-Kubernetes-together.png)

### Kubernetes and Docker

Kubernetes is a container orchestrator for container platforms and it supports 
many containerization tools such as Docker. 

The major difference between Docker 
and Kubernetes is that Docker runs on a single node, whereas Kubernetes is designed 
to run across a cluster.
Another difference between Kubernetes and Docker is 
that Docker can be used without Kubernetes, whereas Kubernetes needs a container 
runtime in order to orchestrate. Kubernetes can act as alternative or a complement 
to Docker.

### Kubernetes concepts

When you deploy Kubernetes, you get a **cluster**. A Kubernetes cluster consists 
of a set of worker machines, called nodes, that run containerized applications. 
Every cluster has at least one worker node.

The **pod** is the basic unit of deployment in Kubernetes. It is a collection of
related containers.

A **node** is basically the virtual machine or server that Kubernetes manages. It 
controls the scheduling of pods across various worker nodes where the application 
actually runs.
The worker nodes host the Pods that are the components of the application workload. 
The control panel manages the worker nodes and the pods in the cluster. In production 
environments, the control plane usually runs across multiple computers, and a cluster 
usually runs multiple nodes, providing fault-tolerance and high availability.

A **service** is an abstract way to expose an application running on a set of 
pods as a network service. Kubernetes gives pods their own IP addresses and a 
single DNS name for a set of Pods, and can load-balance across them.

Kubernetes supports multiple virtual clusters backed by the same physical cluster. 
These virtual clusters are called **namespaces**. They are intended for use in 
environments with many users spread across multiple teams, or projects.

### Kubernetes pros and cons

Pros:

- It's production ready and enterprise grade. It has been used by a lot of companies 
  in production at scale.

- It’s more mature than Docker Swarm.

- Can be used on public, private, hybrid, multi-cloud Modular.

- Self-healing: auto-placement, auto-restart, auto-replication, auto-scaling

- Open-source

- Can be used for any architecture deployment because it's modular.
 

Cons:

- Difficult to set up in a cluster. Most of the cloud providers (like Azure, 
  Google or Amazon) have one click install which works seamlessly.

- Steeper learning curve. It does not use the Docker CLI.

##

# 3. Implementation of the Alternative - Kubernetes

The source code for the alternative solution is located in the folder
**ca4/docker-task-kubernetes** in this
[Bitbucket repository](https://bitbucket.org/raquelcmribeiro/devops-20-21-1200180/src/master/ca4/docker-task/).

## Install tools

To implement an alternative solution for the goals of the ca4 assignment using Kubernetes,
we need to set up the following tools:

- *kubectl* is Kubernetes command-line tool that allows running commands against 
Kubernetes clusters. Use it to deploy applications, inspect and manage 
cluster resources and view logs. Install and set it up 
  [here](https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/).

 
- *minikube* is a tool that allows running Kubernetes locally. minikube runs a 
single-node Kubernetes cluster on the personal computer. Install 
[here](https://minikube.sigs.k8s.io/docs/start/).

 
- [Kompose](https://kompose.io/) is a conversion tool for Docker Compose to 
container orchestrators such as Kubernetes. It simplifies the development process 
with Docker Compose deploying the containers to a production cluster. It allows 
automatic conversion from *docker-compose.yaml* with one simple command. All the 
information, how to install and use
[here](https://kubernetes.io/docs/tasks/configure-pod-container/translate-compose-kubernetes/)

 
## Translate files through Kompose

In the directory containing the **docker-compose.yml** file, open a new terminal 
and use the following command to convert:

```bash
kompose convert --volumes hostPath
```
 

> A hostPath volume mounts a file or directory from the host node's filesystem into the Pod.


A range of files is created based on the *docker-compose.yml* file:

![image](ca4-images/Kompose-convert.PNG)

![image](ca4-images/KomposeGeneratedFiles.PNG)

## Configure Kubernetes resources files

Do not forget to adapt some configurations in the new generated files by Kompose.

In the files **db-deployment.yaml** and **web-deployment.yaml** edit the containers' 
images:

```yaml
spec:
  containers:
    - image: raquelcmribeiro/devops-images:web
```
```yaml
spec:
  containers:
    - image: raquelcmribeiro/devops-images:db
```  
To expose the Services into external IP addresses outside of the cluster in the 
files **web-service.yaml** and **db-service.yaml**, add the port type:

```yaml
spec:
  type: NodePort
```

> **NodePort**: Exposes the Service on each Node's IP at a static port (the NodePort).
You'll be able to contact the NodePort Service, from outside the cluster, by 
requesting *<NodeIP>:<NodePort>*.

Finally in the **default-networkpolicy.yaml** file, change the version:

```yaml
apiVersion: networking.k8s.io/v1
```

## Run deployments in Kubernetes

To start the cluster, in the terminal with administrator access, run:

```bash
minikube start
```

![image](ca4-images/minikubeStart.PNG)

Make sure you're logged in to [DockerHub](https://hub.docker.com/) so the 
container images can be fetched:

```bash
docker login
```

![image](ca4-images/k8s-dockerLogin.PNG)

Finally you're ready to get the pods running by typing the command:

```bash
kubectl apply -f .
```
> This will apply to all files (Kubernetes resources) in the directory. It will 
> not read the *docker-compose.yml* file.

![image](ca4-images/kubectlApply.PNG)

Check if the Pods are running with command:

```bash
kubectl get pods
```

![image](ca4-images/kubectlGetPods.PNG)

## Configure generated IP

First we have to check the generated  internal addresses of the pods that are 
running and get the one for the *db* pod:

```bash
kubectl get pod -o wide
```

![image](ca4-images/kubectlIPs.PNG)

In this case is: **172.17.0.4**.

Now we have to enter in the **web** pod, using its name, to access the project's 
source code:

```bash
kubectl exec --stdin --tty web-9b6d59758-r64h6 -- /bin/bash
```

![image](ca4-images/enter.PNG)

Go to the project's directory in *src/main/resources* and edit the application.properties 
file with the new IP:

> [You may need to install **nano**, or other editor, to edit the file inside the 
> container].

![image](ca4-images/editApplicationProperties1.PNG)

![image](ca4-images/editApplicationProperties.PNG)

- Alternatively you can put the service name instead of the IP address, so it 
  gets mapped dynamically:

![image](ca4-images/applicationPropertiesDynamic.PNG)

Now, since the project was edited, we need to build it again (still inside the pod):

```bash
./gradlew clean build
```

![image](ca4-images/newGradleBuild.PNG)

And copy the new generated *war* file so teh server can run it:

```bash
cp build/libs/tut-basic-gradle-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
```

## Open Application

Now with all set, exit the pod and run the following command to access the external 
IP address for the service:

```bash
minikube service web
```
It will open the default browser:

![image](ca4-images/minikubeServiceWeb.PNG)

And with the given URL and adding the war file name to it, we should be able to 
see the app's frontend working:

![image](ca4-images/kubernetesFrontend.PNG)


##

============================================

*This work was developed by **Raquel Ribeiro [1200180]**.*